#!/bin/bash
script_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

pods=(
  "php"
  "drupal_tools"
  "nginx"
  "varnish"
  "redis"
  "mailhog"
)

declare -A base_images=(
  [php]=php
  [drupal_tools]=php
  [nginx]=nginx
  [varnish]=varnish
  [redis]=redis
  [mailhog]=alpine
  [mariadb]=mariadb
)

declare -A tag_regexes=(
  [php]='^[0-9]+\\.[0-9]+\\.[0-9]+-fpm-alpine[0-9]+\\.[0-9]+'
  [drupal_tools]='^[0-9]+\\.[0-9]+\\.[0-9]+-cli-alpine[0-9]+\\.[0-9]+$'
  [nginx]='^[0-9]+\\.[0-9]+\\.[0-9]+-alpine$'
  [varnish]='^7\\.[0-9]+\\.[0-9]+-alpine$'
  [redis]='^[0-9]+\\.[0-9]+\\.[0-9]+-alpine[0-9]+\\.[0-9]+$'
  [mailhog]='^[0-9]+\\.[0-9]+\\.[0-9]+$'
  [mariadb]='^[0-9]+\\.[0-9]+\\.[0-9]+$'
)

declare -A dockerhub_pages=(
  [php]=4
  [drupal_tools]=4
  [nginx]=1
  [varnish]=1
  [redis]=1
  [mailhog]=1
  [mariadb]=1
)

declare -A latest_tags=()
declare -A build_tags=()
declare -A build_dependend_tags=()

build_images=()

if [ "${1}" == 'dry-run' ]; then
  run_type='--dry-run'
else
  run_type='--results /tmp/drups_build_results/{3}/'
fi

function error() {
  echo -e "\033[0;31m[Error]\033[0m $@" 1>&2
  echo 1>&2
  exit 1
}

function ok() {
  echo -e "\033[0;32m[OK]\033[0m"
  echo
}

function check_result() {
  # If first argument is a number and not 0 or first argument is a empty string then error.
  # Second argument is error message.
  # Pass $? as the first argument to test the result of the previous command.
  # Pass a variable as the first argument to test if it is not empty.
  if  [ -z "$1" ] || [ "$1" -eq "$1" -a "$1" -gt 0 ] 2>/dev/null; then
    error $2
  fi
  ok
}

function get_latest_tag {
  # $1: image
  # $2: tags pagecount
  # $3: result filter regex
  tags=()
  results=()
  for ((i=1; i<=$2; i++)); do
    readarray -t page_results < <(curl -s "https://hub.docker.com/v2/repositories/library/${1}/tags?page=${i}&page_size=100" | jq -r ".results[].name | select(test(\"${3}\"))")
    results=("${results[@]}" "${page_results[@]}")
  done
  if [ ${#results[@]} -eq 0 ]; then
    error "Couldn't retrieve latest ${1} image tags from dockerhub."
  fi
  tags=("${tags[@]}" "${results[@]}")
  tags=($(printf '%s\n' "${tags[@]}"|sort -Vr))
  echo "${tags[0]}"
}

function update_docker_file {
  # $1: build directory (drupal_tools_image)
  # $2: base image
  # $3: latest tag

  # Check if Dockerfile exists and is readable.
  if [ -r "${script_dir}/${1}/Dockerfile" ]; then
    tac "${script_dir}/${1}/Dockerfile" | sed -e "0,/FROM/  s/^FROM .*$/FROM ${2}:${3}/" | tac > "${script_dir}/${1}/Dockerfile.tmp"
    mv "${script_dir}/${1}/Dockerfile.tmp" "${script_dir}/${1}/Dockerfile"
  else
    error "Couldn't read Docker file."
  fi
}

function builders {
  # $1: create / rm
  # $2: number of builders
  existing_builders=$(docker buildx ls)
  if [ "${1}" == "create" ]; then
    echo "Creating builders"
  else
    echo "Removing builders"
  fi
  for i in $(seq 1 $2); do
    if [ "${1}" == "create" ]; then
      if [[ ! "${existing_builders}" == *"builder-${i}"* ]]; then
        if [ "${run_type}" != '--dry-run' ]; then
          docker buildx create --name "builder-${i}"
        fi
      fi
    else
      if [[ "${existing_builders}" == *"builder-${i}"* ]]; then
        if [ "${run_type}" != '--dry-run' ]; then
          docker buildx rm "builder-${i}"
        fi
      fi
    fi
    check_result $?
  done
}

# Get drups images.
echo "Getting current Drups image tags."
if [ -f "${script_dir}/../../drups-images" ]; then
  source "${script_dir}/../../drups-images"
else
  error "Couldn't find drups-images file."
fi

# Declare current image tags.
declare -A current_tags=(
  [php]="${php_image##*:}"
  [drupal_tools]="${drupal_tools_image##*:}"
  [nginx]="${nginx_image##*:}"
  [varnish]="${varnish_image##*:}"
  [redis]="${redis_image##*:}"
  [mailhog]="${mailhog_image##*:}"
  [mariadb]="${mariadb_image##*:}"
)

# Get latest image tags.
echo "Getting latest image tags."
set -e
for pod in "${pods[@]}"; do
  latest_tags[$pod]=$(get_latest_tag ${base_images[$pod]} ${dockerhub_pages[$pod]} ${tag_regexes[$pod]})
  ok
done
latest_tags[mariadb]=$(get_latest_tag mariadb ${dockerhub_pages[$pod]} ${tag_regexes[$pod]})
set +e

# Define updates.
printf "%-22s %-22s    %-22s\n" "Pod" "Current" "Latest"
printf "%-22s-%-22s----%-22s\n" "----------------------" "----------------------" "----------------------"
for pod in "${pods[@]}"; do
  if [ "${current_tags[$pod]}" != "${latest_tags[$pod]}" ]; then
    printf "%-22s %-22s => %-22s\n" "${pod}" "${current_tags[$pod]}" "${latest_tags[$pod]}"
    build_tags[$pod]="${latest_tags[$pod]}"
  else
    printf "%-22s %-22s    %-22s\n" "${pod}" "${current_tags[$pod]}" "${latest_tags[$pod]}"
  fi
done
# Mariadb image.
if [ "${current_tags[mariadb]}" != "${latest_tags[mariadb]}" ]; then
  printf "%-22s %-22s => %-22s\n" "mariadb" "${current_tags[mariadb]}" "${latest_tags[mariadb]}"
else
  printf "%-22s %-22s    %-22s\n" "mariadb" "${current_tags[mariadb]}" "${latest_tags[mariadb]}"
fi

# Always build varnish.
build_tags[varnish]="${latest_tags[varnish]}"


# Ask for confirmation.
echo
read -p "Build images? [y/n]" -n 1 -r
echo
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
  exit
fi
echo

# Update Docker files.
echo "Updating Docker files."
for image in "${!build_tags[@]}"; do
  update_docker_file "${image}" "${base_images[$image]}" "${build_tags[$image]}"
done
ok

# Prepare build.
docker run --rm --privileged docker/binfmt:820fdd95a9972a5308930a2bdfb8573dd4447ad3
builders create "${#build_tags[@]}"

# Cleanup old logs.
rm -rf /tmp/drups_build_results
mkdir /tmp/drups_build_results

# Build images.
echo "Building images."
image_name_parts="${!build_tags[@]}"
image_name_parts="${image_name_parts//_/-}"
parallel --jobs 0 --tagstring {3} ${run_type} --halt now,fail=1 "docker buildx build --push --builder builder-{4} --platform linux/arm,linux/arm64,linux/amd64 --no-cache --tag thomasdegraaff/drups-{1}:{2} ${script_dir}/{3}/" ::: ${image_name_parts} :::+ ${build_tags[@]} :::+ ${!build_tags[@]} :::+ $(seq 1 ${#build_tags[@]})
if [ "$?" -eq 0 ]; then
  ok
fi

# Cleanup builders.
builders rm "${#build_tags[@]}"

# Dependend images:
if [ "${run_type}" != '--dry-run' ]; then
  echo "Wait a minute for pushed images to settle."
  sleep 60
fi

echo "Updating dependend Dockerfiles."
# Lando php image.
if [ ! -z "${build_tags[php]}" ]; then
  update_docker_file "lando_php" "thomasdegraaff\/drups-php" "${build_tags[php]}"
  ok
  build_dependend_tags['lando_php']="${build_tags['php']}"
fi

# Lando tools image.
if [ ! -z "${build_tags[drupal_tools]}" ]; then
  update_docker_file "lando_tools" "thomasdegraaff\/drups-drupal-tools" "${build_tags[drupal_tools]}"
  ok
  build_dependend_tags['lando_tools']="${build_tags['drupal_tools']}"
fi

# Prepare build.
builders create "${#build_dependend_tags[@]}"

echo "Building dependend images."
image_name_parts="${!build_dependend_tags[@]}"
image_name_parts="${image_name_parts//_/-}"

parallel --jobs 0 --tagstring {3} ${run_type} --halt now,fail=1 "docker buildx build --push --builder builder-{4} --platform linux/arm,linux/arm64,linux/amd64 --no-cache --tag thomasdegraaff/drups-{1}:{2} ${script_dir}/{3}/" ::: ${image_name_parts} :::+ ${build_dependend_tags[@]} :::+ ${!build_dependend_tags[@]} :::+ $(seq 1 ${#build_dependend_tags[@]})
if [ "$?" -eq 0 ]; then
  ok
fi

# Cleanup builders.
builders rm "${#build_dependend_tags[@]}"

# Update drups-images file.
if [ "${run_type}" != '--dry-run' ]; then
  echo "# Drups Docker images." > "${script_dir}/../../drups-images"
fi
for pod in "${pods[@]}"; do
  if [ -z "${build_tags[$pod]}" ]; then
    if [ "${run_type}" == '--dry-run' ]; then
      echo "${pod}_image='thomasdegraaff/drups-${pod//_/-}:${current_tags[$pod]}'"
    else
      echo "${pod}_image='thomasdegraaff/drups-${pod//_/-}:${current_tags[$pod]}'" >> "${script_dir}/../../drups-images"
    fi
  else
    if [ "${run_type}" == '--dry-run' ]; then
      echo "${pod}_image='thomasdegraaff/drups-${pod//_/-}:${build_tags[$pod]}'"
    else
      echo "${pod}_image='thomasdegraaff/drups-${pod//_/-}:${build_tags[$pod]}'" >> "${script_dir}/../../drups-images"
    fi
  fi
done
# Dependend images.
if [ ! -z "${build_tags[php]}" ]; then
  if [ "${run_type}" == '--dry-run' ]; then
    echo "lando_php_image='thomasdegraaff/drups-lando-php:${build_tags[php]}'"
  else
    echo "lando_php_image='thomasdegraaff/drups-lando-php:${build_tags[php]}'" >> "${script_dir}/../../drups-images"
  fi
else
  if [ "${run_type}" == '--dry-run' ]; then
    echo "lando_php_image='thomasdegraaff/drups-lando-php:${current_tags[php]}'"
  else
    echo "lando_php_image='thomasdegraaff/drups-lando-php:${current_tags[php]}'" >> "${script_dir}/../../drups-images"
  fi
fi
if [ ! -z "${build_tags[drupal_tools]}" ]; then
  if [ "${run_type}" == '--dry-run' ]; then
    echo "lando_tools_image='thomasdegraaff/drups-lando-tools:${build_tags[drupal_tools]}'"
  else
    echo "lando_tools_image='thomasdegraaff/drups-lando-tools:${build_tags[drupal_tools]}'" >> "${script_dir}/../../drups-images"
  fi
else
  if [ "${run_type}" == '--dry-run' ]; then
    echo "lando_tools_image='thomasdegraaff/drups-lando-tools:${current_tags[drupal_tools]}'"
  else
    echo "lando_tools_image='thomasdegraaff/drups-lando-tools:${current_tags[drupal_tools]}'" >> "${script_dir}/../../drups-images"
  fi
fi
# Mariadb.
if [ "${current_tags[mariadb]}" == "${latest_tags[mariadb]}" ]; then
  if [ "${run_type}" == '--dry-run' ]; then
    echo "mariadb_image='mariadb:${current_tags[mariadb]}'"
  else
    echo "mariadb_image='mariadb:${current_tags[mariadb]}'" >> "${script_dir}/../../drups-images"
  fi
else
  if [ "${run_type}" == '--dry-run' ]; then
    echo "mariadb_image='mariadb:${latest_tags[mariadb]}'"
  else
    echo "mariadb_image='mariadb:${latest_tags[mariadb]}'" >> "${script_dir}/../../drups-images"
  fi
fi