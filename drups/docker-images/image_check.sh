#!/bin/bash
script_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

images=(
  "php"
  "lando_php"
  "drupal_tools"
  "lando_tools"
  "nginx"
  "varnish"
  "redis"
  "mailhog"
)

declare -A base_images=(
  [php]=php
  [lando_php]=php
  [drupal_tools]=php
  [lando_tools]=php
  [nginx]=nginx
  [varnish]=varnish
  [redis]=redis
  [mailhog]=alpine
  [mariadb]=mariadb
)

function error() {
  echo -e "\033[0;31m[Error]\033[0m $@" 1>&2
  echo 1>&2
  exit 1
}

function ok() {
  echo -e "\033[0;32m[OK]\033[0m"
  echo
}

function check_result() {
  # If first argument is a number and not 0 or first argument is a empty string then error.
  # Second argument is error message.
  # Pass $? as the first argument to test the result of the previous command.
  # Pass a variable as the first argument to test if it is not empty.
  if  [ -z "$1" ] || [ "$1" -eq "$1" -a "$1" -gt 0 ] 2>/dev/null; then
    error $2
  fi
  ok
}

# Get drups images.
echo "Getting current Drups image tags."
if [ -f "${script_dir}/../../drups-images" ]; then
  source "${script_dir}/../../drups-images"
  ok
else
  error "Couldn't find drups-images file."
fi

# Declare current image tags.
declare -A tags=(
  [php]="${php_image##*:}"
  [lando_php]="${lando_php_image##*:}"
  [drupal_tools]="${drupal_tools_image##*:}"
  [lando_tools]="${lando_tools_image##*:}"
  [nginx]="${nginx_image##*:}"
  [varnish]="${varnish_image##*:}"
  [redis]="${redis_image##*:}"
  [mailhog]="${mailhog_image##*:}"
  [mariadb]="${mariadb_image##*:}"
)

# Define updates.
echo -e "\nCheck the following images for security issues:\n"
printf "%-22s %-22s    %-22s\n" "Image" "Tag"
printf "%-22s-%-22s----%-22s\n" "----------------------" "----------------------"
for image in "${images[@]}"; do
  printf "%-22s %-22s    %-22s\n" "thomasdegraaff/drups-${image/_/-}" "${tags[$image]}"
done

# Check images.
for image in "${images[@]}"; do
  trivy image "thomasdegraaff/drups-${image/_/-}:${tags[$image]}"
done
