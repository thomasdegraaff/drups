#!/bin/sh
set -e

# Remove default config.
rm -f /etc/nginx/conf.d/default.conf
# Use template to create nginx config.
envsubst '${NGINX_ROOT_PATH} ${NGINX_PHP_SERVER}' < /etc/nginx/site.conf.tpl > /etc/nginx/conf.d/site.conf