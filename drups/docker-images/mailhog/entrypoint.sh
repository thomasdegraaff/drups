#!/bin/sh
set -e

export MH_AUTH_FILE=/home/mailhog/mhauth

if [ ! -z "${MAILHOG_USER}" ] && [ ! -z "${MAILHOG_PASSWORD}" ]; then
  # Create mailhog auth file.
  echo "${MAILHOG_USER}:$(MailHog bcrypt ${MAILHOG_PASSWORD})" > "${MH_AUTH_FILE}"
fi

MailHog &

# this will check if there are arguments.
if [ "$#" -eq 0 ]; then
  # No arguments, then keep on running.
  while true; do sleep 5; done;
fi

exec "$@"