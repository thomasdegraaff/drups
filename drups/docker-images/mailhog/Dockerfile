#
# MailHog Dockerfile
#

FROM golang:alpine as builder

# Install MailHog:
RUN apk --no-cache add --virtual build-dependencies \
    git \
  && mkdir -p /root/gocode \
  && GOPATH=/root/gocode GOMAXPROCS=1 go install github.com/mailhog/MailHog@latest

FROM alpine:3.16.0
# Add mailhog user/group with uid/gid 1000.
# This is a workaround for boot2docker issue #581, see
# https://github.com/boot2docker/boot2docker/issues/581
RUN adduser -D -u 1000 mailhog

COPY --from=builder /root/gocode/bin/MailHog /usr/local/bin/

COPY entrypoint.sh /usr/local/bin/entrypoint.sh

USER mailhog

WORKDIR /home/mailhog

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
EXPOSE 1025 8025
