Environment variables:
```
$VARNISH_BACKEND_DEFAULT # Default backend
$VARNISH_PURGE_HOST      # Purge host
$VARNISH_SIZE            # 1G, 200M, 200K
```