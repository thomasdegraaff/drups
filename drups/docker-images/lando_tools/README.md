Environment vars:

```
RUN_CRON
SSH_PRIVATE_KEY
PHP_MEMORY_LIMIT
PHP_MAX_EXECUTION_TIME
PHP_UPLOAD_MAX_FILESIZE
PHP_POST_MAX_SIZE
SENDMAIL_PORT
SENDMAIL_HOST
```

```RUN_CRON```
When 'true' then run crond.

```SSH_PRIVATE_KEY```
Private key file for git repository access.