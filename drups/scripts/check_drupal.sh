#!/bin/bash
if [ -z "${site_dir}" ]; then
  source /app/drups/scripts/includes.sh
fi

# Check if composer.json exists.
check_composer_json

# Check if drupal is running.
drupal_running=false
echo -n "Waiting for running Drupal instance. "
while [ "$drupal_running" == "false" ]; do
  if /app/vendor/bin/drush status bootstrap | grep -q Successful; then
    drupal_running=true
    echo "Running Drupal instance found."
    ok
  else
    sleep 10
    echo -n '.'
  fi
done
echo

if [ "$drupal_running" = false ]; then
  error "Database error."
fi