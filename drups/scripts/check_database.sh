#!/bin/bash
if [ -z "${site_dir}" ]; then
  source /app/drups/scripts/includes.sh
fi

# Check database connection.
  connect_mysql=false
  drupal_database=false

  echo -n "Waiting for database connection. "
  while [ "$connect_mysql" == "false" ]; do
    if [ "$(mysql -u drupal "-p${MARIADB_PASSWORD}" -h "${MARIADB_HOST}" -ss -e "SHOW DATABASES LIKE 'information_schema';" 2>/dev/null)" = 'information_schema' ]; then
      connect_mysql=true
      # Database connection succesful.
      echo -e "\nDatabase connection succesful."
      echo -n "Waiting for drupal database. "
      j="0"
      # Check if drupal database exists.
      while [ "$drupal_database" == "false" ]; do
        if [ "$(mysql -u drupal "-p${MARIADB_PASSWORD}" -h "${MARIADB_HOST}" -ss -e "SHOW DATABASES LIKE 'drupal';")" = 'drupal' ]; then
          drupal_database=true
          echo
        else
          sleep 2; echo -n '.'
        fi
      done
      connect_mysql=true
    else
      sleep 2; echo -n '.'
    fi
  done

  sleep 5