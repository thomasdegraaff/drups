#!/bin/bash
set -o pipefail

includes_loaded='true'

drupal_root="/app/web"
site_dir="/app/web/sites/default"
drupal_settings_dir="/app/drupal/settings"
drupal_config_dir="/app/drupal/config/sync"
store_dir="/app/storage"
helm_dir="/app/kube/helm/drups"
tooling_script_dir="/app/lando/scripts/tooling"

# Get default values.
source /app/drups-defaults

# Get image versions.
source /app/drups-images

function error() {
  echo -e "\033[0;31m[Error]\033[0m $@" 1>&2
  echo
  exit 1
}

function ok() {
  echo -e "\033[0;32m[OK]\033[0m"
  echo
}

function check_result() {
  # If first argument is a number and not 0 or first argument is a empty string then error.
  # Second argument is error message.
  # Pass $? as the first argument to test the result of the previous command.
  # Pass a variable as the first argument to test if it is not empty.
  if  [ -z "$1" ] || [ "$1" -eq "$1" -a "$1" -gt 0 ] 2>/dev/null; then
    error $2
  fi
  ok
}

function check_composer_json() {
  if [ -z "${composer_json_checked}" ]; then
    # Check if composer.json file exists.
    echo "Checking if composer.json file exists."
    if [ ! -f "/app/composer.json" ]; then
      error "Unable to use drush, missing composer.json file."
    else
      composer_json_checked='true'
      ok
    fi
  fi
}

cd /app
