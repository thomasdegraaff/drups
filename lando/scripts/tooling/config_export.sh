#!/bin/bash
source /app/lando/scripts/tooling/includes.sh

# Check if comoposer.json exists.
check_composer_json

# Export drupal config.
echo "Exporting Drupal configuration."
cd "${site_dir}" && /app/vendor/bin/drush -y --root=/app config:export
check_result $?