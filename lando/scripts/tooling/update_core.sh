#!/bin/bash
if [ ! "${includes_loaded}" == 'true' ]; then
  source /app/drups/scripts/includes.sh
fi

# Check for core updates.
if composer outdated "drupal/*" | grep -q 'drupal/core-recommended'; then
  # Check if composer.json exists.
  check_composer_json
  # Update drupal core.
  echo -e "\nCore update found. Updating Drupal core."
  composer update drupal/core "drupal/core-*" --with-all-dependencies
  echo -e "\nUpdating database."
  /app/vendor/bin/drush updatedb
  echo -e "\nRebuilding caches."
  /app/vendor/bin/drush cache:rebuild
else
  # No core update found.
  echo -e "\nNo core update found."
fi

echo "Done"