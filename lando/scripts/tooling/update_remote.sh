#!/bin/bash
source /app/lando/scripts/tooling/includes.sh

# Check uncommitted changes.
check_uncommitted

# Check if repo contains composer.json.
echo "Check if repository contains composer files."
git fetch "${git_origin}" > /dev/null 2>&1
git cat-file -e "${git_origin}/${git_branch}:composer.json" 2>/dev/null
check_result $? "Git repository lacks composer.json file.\nUpdating remote code now would break drush in remote deployment.\nMake sure composer.json is committed and pushed, and then try again."

# Check if kubeconfig exists.
check_cluster_accessible "${kube_context}"

# Check deployment.
check_deployment

# Set deployment in maintenance mode.
set_maintenance_mode "${pod_prefix}" "${site_url}" 1 "${kube_context}"

# Get drupal-tools pod name.
echo "Getting drupal-tools pod name."
drupal_tools_pod_name="$(get_pod_name "${pod_prefix}" 'drupal-tools' "${kube_context}")"

# Get latest code.
echo "Get latest code from repo."
execute_job "${drupal_tools_pod_name}" 'drupal-tools' 'git clean -f && git pull' "${kube_context}"

# Clear caches.
echo "Clearing Drupal caches."
execute_job "${drupal_tools_pod_name}" 'drupal-tools' '/app/vendor/bin/drush cache:rebuild' "${kube_context}"

# Get latest code.
echo "Update Drupal code."
execute_job "${drupal_tools_pod_name}" 'drupal-tools' 'composer install' "${kube_context}"

# Update Drupal.
echo "Running updates."
execute_job "${drupal_tools_pod_name}" 'drupal-tools' '/app/kube/scripts/drupal_install.sh' "${kube_context}"
check_result $?

# Get source deployment out of maintenance mode.
set_maintenance_mode "${pod_prefix}" "${site_url}" 0 "${kube_context}"

# Clear varnish cache.
varnish_cache_clear "${git_branch}" "${pod_prefix}" "${kube_context}"
