#!/bin/bash
source /app/lando/scripts/tooling/includes.sh

# Check for uncommitted code.
check_uncommitted

# Check if kubeconfig exists.
check_cluster_accessible "${kube_context}"

# Check if deployment is ok.
check_deployment

# Push data.
push_data "${kube_context}"

# Get target deployment out of maintenance mode.
set_maintenance_mode "${pod_prefix}" "${site_url}" 0 "${kube_context}"