if [ ! "${includes_loaded}" == 'true' ]; then
  source /app/drups/scripts/includes.sh
fi

/app/vendor/bin/phpcs -i

# Check if custom modules.
if ls "${drupal_root}"/modules/custom/*/*.module >/dev/null 2>&1; then
  echo -e "\nCustom modules found."

  # Check Drupal coding standards.
  echo -e "\nChecking Drupal coding standards."
  phpcs --standard=Drupal --extensions=php,module,inc,install,test,profile,theme,css,info,txt,md,yml --ignore=node_modules,bower_components,vendor "${drupal_root}/modules/custom"

  # Check Drupal best practices.
  echo -e "\nChecking Drupal best practices."
  phpcs --standard=DrupalPractice --extensions=php,module,inc,install,test,profile,theme,css,info,txt,md,yml --ignore=node_modules,bower_components,vendor "${drupal_root}/modules/custom"

  # Phpstan checks.
  echo -e "\nPhpstan code check."
  phpstan analyse -c /app/phpstan.neon "${drupal_root}/modules/custom"

else
  echo -e "\nNo custom modules found."
fi

# Check if custom themes.
if ls "${drupal_root}"/themes/custom/*/*.module >/dev/null 2>&1; then
  echo -e "\nCustom themes found."

  # Check Drupal coding standards.
  echo -e "\nChecking Drupal coding standards."
  phpcs --standard=Drupal --extensions=php,module,inc,install,test,profile,theme,css,info,txt,md,yml --ignore=node_modules,bower_components,vendor "${drupal_root}/themes/custom"

  # Check Drupal best practices.
  echo -e "\nChecking Drupal best practices."
  phpcs --standard=DrupalPractice --extensions=php,module,inc,install,test,profile,theme,css,info,txt,md,yml --ignore=node_modules,bower_components,vendor "${drupal_root}/themes/custom"

else
  echo -e "\nNo custom themes found."
fi