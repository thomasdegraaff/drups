#!/bin/bash
source /app/drups/scripts/includes.sh

# Check if composer.json file exists.
check_composer_json

echo "Import data."

# Get current commit hash.
git config --global --add safe.directory /app
git_hash="$(git rev-parse HEAD)"

# Check if Drupal is running.
/app/drups/scripts/check_database.sh
if /app/vendor/bin/drush status bootstrap | grep -q Successful; then
  # Drupal is running.
  echo "Drupal is already installed, not importing data."
else
  # Check if storage exists.
  if [ -f "${store_dir}/database-${git_hash}.gz" ] && [ -f "${store_dir}/files-${git_hash}.tar.gz" ]; then
    # Restore site data.
    /app/lando/scripts/tooling/push_data_local.sh
  fi
fi
