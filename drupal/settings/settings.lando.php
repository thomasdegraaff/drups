<?php

# Include local settings.
if (file_exists($app_root . '/' . $site_path . '/settings.local.php')) {
  include $app_root . '/' . $site_path . '/settings.local.php';
}

// Trusted host patterns.
$settings['trusted_host_patterns'][] = '^.+$';

// Set swiftmailer to use mailhog.
$config['swiftmailer.transport'] = [
  'transport' => 'smtp',
  'smtp_host' => 'sendmailhog',
  'smtp_port' => 1025,
  'smtp_encryption' => '0',
  'smtp_credential_provider' => 'swiftmailer',
];
