# Drups

To build and configure your own Drupal devops environment is a lot of work. Drups makes this a breeze by using available open source tools to automate the setup and configuration of your own Drupal devops environment.

[Lando](https://lando.dev/) is a great project for local Drupal development. It is a wrapper around Docker that makes it really easy to spin up the needed containers and development tooling on your workstation.

[Kubernetes](https://kubernetes.io/) makes it easy to spin up containers in a controlled cluster environment and publish services on the internet.

Drups connects these two tools to create and configure a convenient Drupal development experience. Develop a project branch on your workstation, and then publish it in a Kubernetes cluster for reviewing, testing, production etc.

You can build your own cluster using cheap Raspberry Pi hardware, use a third party managed Kubernetes cluster solution, or do both.

## Features
* Develop on your local workstation anywhere.
* Multi cluster support, use separate clusters for your development and live deployments.
* Easily deploy your site's branches in your cluster and publish them on the internet.
* Exactly the same server environment for local development and cluster deployments.
* Drupal caching configured and enabled out of the box.
* Secure out of the box (Letsencrypt certificates and secure headers configured).
* Site storage backups and restores.
* Use a git workflow to work together on projects in a team.
* Development tools configured (Xdebug, phpstan, codesniffer).
* High availability, one cluster node (Raspberry Pi) down (for maintenance), other nodes will take over.
* Completely open source, and easily customizable with basic linux knowledge.

Go to the [drups.eu](https://drups.eu) website to see how to setup a Drups cluster and how to use Drups for your Drupal development.